/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test

import android.util.Log
import io.camlcase.javatezos.TezosNodeClient
import io.camlcase.javatezos.michelson.MichelsonComparable
import io.camlcase.javatezos.michelson.MichelsonParameter
import io.camlcase.javatezos.michelson.StringMichelsonParameter
import io.camlcase.javatezos.model.*
import io.camlcase.javatezos.network.DefaultNetworkClient
import io.camlcase.javatezos.operation.OperationFeesPolicy
import io.camlcase.javatezos.operation.OperationMetadataService
import io.camlcase.javatezos.test.util.BabylonAddress.Companion.TEST_ADDRESS_TZ1_WRO
import io.github.vjames19.futures.jdk8.onComplete
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.Executors

class TezosNodeClientRealTest {

    private fun testHead() {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.getHead(object : TezosCallback<Map<String, Any>> {
            override fun onSuccess(item: Map<String, Any>?) {
                println("** SUCCESS! $item")
            }

            override fun onFailure(error: TezosError) {
                println("** ERROR! $error")
            }
        })
    }

    private fun testHeadHash() {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.getHeadHash(object : TezosCallback<String> {
            override fun onSuccess(item: String?) {
                println("** SUCCESS! $item")
            }

            override fun onFailure(error: TezosError) {
                println("** ERROR! $error")
            }
        })
    }

    private fun testGetBalance(address: Address = TEST_ADDRESS_TZ1_WRO) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.getBalance(address, object : TezosCallback<Tez> {
            override fun onSuccess(item: Tez?) {
                Log.i("** SUCCESS! ", "$item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                Log.i("** ERROR! ", "$error")
            }
        })
    }

    private fun testOperationMetadata() {
        val executorService = Executors.newFixedThreadPool(3)
        val service =
            OperationMetadataService(
                DefaultNetworkClient(TezosNodeClient.CARTHAGENET_CRYPTIUM_NODE),
                executorService
            )
        service.getMetadata(TEST_ADDRESS_TZ1_WRO)
            .onComplete(
                onFailure = { throwable ->
                    println("** ERROR! $throwable")
                },
                onSuccess = { result ->
                    println("** SUCCESS! $result - ${Thread.currentThread()}")
                })
    }

    /**
     * You first need to check the account has enough XTZ
     * https://babylonnet.tezos.id/accounts/[from]
     */
    private fun testSendTransaction(
        from: Wallet? = bakerWallet1,
        to: Address = bakerWallet2!!.address,
        amount: Tez = Tez("1")
    ) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.send(
            amount,
            to,
            from!!.address,
            from,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** ERROR! $error")
                }
            })
    }

    private fun testOriginateAccount(account: Wallet? = bakerWallet2) {
        println(account)
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.originateAccount(
            account!!.address,
            account,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** ERROR! $error")
                }
            })
    }

    private fun testGetDelegate(address: Address = bakerWallet1!!.address) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.getDelegate(address, object : TezosCallback<String> {
            override fun onSuccess(item: String?) {
                println("** SUCCESS! $item")
            }

            override fun onFailure(error: TezosError) {
                println("** ERROR! $error")
            }
        })
    }

    /**
     * Check the delegate has balance
     */
    private fun testRegisterDelegate(delegate: Wallet? = bakerWallet2) {
        println(delegate)
        val client = TezosNodeClient(LOCAL_NODE_URL)

        client.registerDelegate(
            delegate!!.address,
            delegate,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DELEGATE SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DELEGATE ERROR! $error")
                }
            })
    }

    /**
     * [source] should not be registered as baker/delegate
     */
    private fun testDelegate(
        source: Wallet? = delegatorWallet,
        delegate: Address = bakerWallet1!!.address
    ) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.delegate(
            source!!.address,
            delegate,
            source,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DELEGATE SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DELEGATE ERROR! $error")
                }
            })
    }

    private fun testReveal(source: Wallet? = Wallet()) {
        println(source)
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.reveal(
            source!!.address,
            source.publicKey,
            source,
            OperationFeesPolicy.Default(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** ERROR! $error")
                }
            })
    }

    private fun testCallSmartContact(
        source: Wallet? = bakerWallet1,
        contract: Address = TEST_SAMPLE_CONTRACT,
        amount: Tez = Tez.zero,
        parameter: MichelsonParameter? = StringMichelsonParameter("world")
    ) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.call(
            source!!.address,
            contract,
            amount,
            parameter,
            source,
            OperationFeesPolicy.Estimate(),
            object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** SMART CONTRACT SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** SMART CONTRACT ERROR! $error")
                }
            })
    }

    fun testContractStorage(contract: Address = TEST_SAMPLE_CONTRACT) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.getContractStorage(contract, object : TezosCallback<Map<String, Any>> {
            override fun onSuccess(item: Map<String, Any>?) {
                println("** STORAGE SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                println("** STORAGE ERROR! $error")
            }
        })
    }

    fun testGetBigMapValue(
        contract: Address = "KT1LKSFTrGSDNfVbWV4JXRrqGRD8XDSv5NAU",
        key: MichelsonParameter = StringMichelsonParameter("tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW"),
        type: MichelsonComparable = MichelsonComparable.ADDRESS
    ) {
        val client = TezosNodeClient(LOCAL_NODE_URL)
        client.getBigMapValue(contract, key, type, object : TezosCallback<Map<String, Any>> {
            override fun onSuccess(item: Map<String, Any>?) {
                println("** BIGMAP SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                println("** BIGMAP ERROR! $error")
            }
        })
    }

    private fun waitForInclusion() {
        Thread.sleep(90 * 1000) // wait ~1minute for the tx to be included in the chain
    }

    /**
     * Test against a real node
     */
    @Test
    fun testRealNode() {


        // Let the connection return something
        // Thread.sleep(10*1000)
    }
}

const val LOCAL_NODE_URL = "http://127.0.0.1:8732"

const val TEST_ORIGINATED_ADDRESS = "KT1D5jmrBD7bDa3jCpgzo32FMYmRDdK2ihka"
const val TEST_DEXTER_EXCHANGE_CONTRACT = "KT1RrfbcDM5eqho4j4u5EbqbaoEFwBsXA434"
const val TEST_SAMPLE_CONTRACT = "KT194hQVCPCsCYUhDjbeUCwpKcKyzN3AZLbc" // idString.tz
const val TEST_SAMPLE_CONTRACT_WRAPPER = "KT1X3xbxVotmjjUe6MF5vD4Su4k7XKmiRrBG" // stringCaller.tz

// tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW
val sharedWithKitWallet = Wallet(
    listOf(
        "predict",
        "corn",
        "duty",
        "process",
        "brisk",
        "tomato",
        "shrimp",
        "virtual",
        "horror",
        "half",
        "rhythm",
        "cook"
    )
)
// tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B
val bakerWallet1 = Wallet(
    "unaware, shiver, shrug, merry, design, blue, feature, cost, glimpse, donate, conduct, index, time, around, dose, miss, shoot, marble, valve, syrup, cloud, intact, note, benefit, provide, tiger, come, future, stuff, wise, fix, mountain, knife, cricket, there, cactus, ostrich, dignity, pizza, satoshi, manual, sword, raven, lemon, useful, demise, wage, friend, flash, frame, reduce, enlist, sentence, soda, ship, admit, candy, couple, have, apple, prepare, slice, rug, enemy, almost, voyage, merge, refuse, parrot, consider, hold, medal, bone, help, erase, injury, pass, glove, drill, echo, pulse, flash, chief, lend, crack, midnight, hand, shiver, run, comic, reduce, calm, slide, damage, track, author"
        .split(", ")
)
// tz1eZAGXmXxwkXUBUxuSk5XkJ5UZ5Q25Baja
val bakerWallet2 = Wallet(
    "general, pottery, brain, snow, benefit, trumpet, someone, fly, again, smooth, library, type, quiz, bleak, wrist, roast, normal, black, mercy, method, satoshi, point, collect, shield, point, vicious, aisle, upset, scorpion, spirit, mom, choose, lazy, bargain, rifle, crumble, drift, access, shop, track, orchard, social, alley, lunch, speak, extra, record, right, soft, screen, ostrich, hire, razor, insect, negative, amount, bus, shop, kiss, riot, ramp, laugh, jungle, endorse, hammer, fat, harvest, error, board, obey, april, reward, rival, cause, rocket, brother, train, young, wrong, clinic, vapor, birth, face, strike, sleep, bike, bar, identify, junk, conduct, green, damp, wheat, what, wrap, dismiss"
        .split(", ")
)

// tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz
val delegatorWallet = Wallet(
    "soccer, vacant, bonus, salon, neglect, try, enroll, together, prevent, tenant, route, fragile, betray, vanish, into, enhance, element, economy, absent, speed, mixed, earth, already, ready, again, insect, oyster, marriage, tower, nest, knock, fashion, actual, faint, trouble, camp, bring, beef, expire, tumble, gather, venue, stamp, quote, market, bid, foam, train, address, punch, forget, trust, hill, want, genre, illness, cover, bench, peasant, network, venue, heavy, point, area, since, turn, moral, cradle, phrase, success, fantasy, cause, object, dove, achieve, salt, play, market, left, carbon, pause, silent, season, add, menu, month, sound, jump, stairs, patrol, beyond, family, expose, reopen, antenna, kingdom"
        .split(", ")
)

// tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz
val testWallet = Wallet(
    "soccer, vacant, bonus, salon, neglect, try, enroll, together, prevent, tenant, route, fragile, betray, vanish, into, enhance, element, economy, absent, speed, mixed, earth, already, ready, again, insect, oyster, marriage, tower, nest, knock, fashion, actual, faint, trouble, camp, bring, beef, expire, tumble, gather, venue, stamp, quote, market, bid, foam, train, address, punch, forget, trust, hill, want, genre, illness, cover, bench, peasant, network, venue, heavy, point, area, since, turn, moral, cradle, phrase, success, fantasy, cause, object, dove, achieve, salt, play, market, left, carbon, pause, silent, season, add, menu, month, sound, jump, stairs, patrol, beyond, family, expose, reopen, antenna, kingdom"
        .split(", ")
)
