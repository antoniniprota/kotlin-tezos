/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.model

import io.camlcase.javatezos.model.Wallet
import io.camlcase.javatezos.test.crypto.mnemonic.MnemonicFacadeTest.Companion.MNEMONIC
import org.junit.Assert
import org.junit.Test

class WalletTest {
    @Test
    fun testGenerateWallet() {
        val wallet = Wallet()
        Assert.assertNotNull(wallet)
        Assert.assertNotNull(wallet!!.mnemonic)
        Assert.assertNotNull(wallet!!.publicKey)
        Assert.assertNotNull(wallet!!.address)
    }

    @Test
    fun testGenerateWalletMnemonicNoPassphrase() {
        val wallet = Wallet(MNEMONIC)
        Assert.assertNotNull(wallet)
        Assert.assertEquals(PUBLIC_KEY_NO_PASSPHRASE, wallet!!.publicKey.base58Representation)
        Assert.assertEquals(PUBLIC_KEY_HASH_NO_PASSPHRASE, wallet!!.publicKey.hash)
        Assert.assertEquals(SECRET_KEY_NO_PASSPHRASE, wallet!!.secretKey.base58Representation)
    }

    @Test
    fun testGenerateWalletMnemonicEmptyPassphrase() {
        val wallet = Wallet(MNEMONIC, "")
        Assert.assertNotNull(wallet)
        // A wallet with an empty passphrase should be the same as a wallet with no passphrase.
        Assert.assertEquals(PUBLIC_KEY_NO_PASSPHRASE, wallet!!.publicKey.base58Representation)
        Assert.assertEquals(PUBLIC_KEY_HASH_NO_PASSPHRASE, wallet!!.publicKey.hash)
        Assert.assertEquals(SECRET_KEY_NO_PASSPHRASE, wallet!!.secretKey.base58Representation)
    }

    @Test
    fun testGenerateWalletMnemonicWithPassphrase() {
        val wallet = Wallet(MNEMONIC, "TezosKitTest")
        Assert.assertNotNull(wallet)
        Assert.assertEquals(PUBLIC_KEY_PASSPHRASE, wallet!!.publicKey.base58Representation)
        Assert.assertEquals(PUBLIC_KEY_HASH_PASSPHRASE, wallet!!.publicKey.hash)
        Assert.assertEquals(SECRET_KEY_PASSPHRASE, wallet!!.secretKey.base58Representation)
    }


    companion object{
        // Expected outputs for a wallet without a passphrase.
        const val PUBLIC_KEY_NO_PASSPHRASE = "edpku9ZF6UUAEo1AL3NWy1oxHLL6AfQcGYwA5hFKrEKVHMT3Xx889A"
        const val SECRET_KEY_NO_PASSPHRASE =
        "edskS4pbuA7rwMjsZGmHU18aMP96VmjegxBzwMZs3DrcXHcMV7VyfQLkD5pqEE84wAMHzi8oVZF6wbgxv3FKzg7cLqzURjaXUp"
        const val PUBLIC_KEY_HASH_NO_PASSPHRASE = "tz1Y3qqTg9HdrzZGbEjiCPmwuZ7fWVxpPtRw"

        // Expected outputs for a wallet with a passphrase.
        const val PUBLIC_KEY_PASSPHRASE = "edpktnCgi3C7ZLyLrF4NAebDkgu5PZRRJ9BafxskVEj6U1GycyRird"
        const val SECRET_KEY_PASSPHRASE =
        "edskRjazzmroxmJagYDhCT1jXna8m9H2qvjtPAcrZYZ31og4ud1u2kkxYGv8e7CjmbW33QubzugueXqLFPMbM2eAj6j3AQHrCW"
        const val PUBLIC_KEY_HASH_PASSPHRASE = "tz1ZfhME1B2kmagqEJ9P7PE8joM3TbVQ5r4v"
    }
}
