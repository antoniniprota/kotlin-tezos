/**
 * # Released under MIT License
 * <p>
 * Copyright (c) 2019 camlCase
 * <p>
 */
package io.camlcase.javatezos.test.integration;

import io.camlcase.javatezos.TezosNodeClient;
import io.camlcase.javatezos.michelson.MichelsonComparable;
import io.camlcase.javatezos.michelson.StringMichelsonParameter;
import io.camlcase.javatezos.model.Tez;
import io.camlcase.javatezos.model.TezosCallback;
import io.camlcase.javatezos.model.TezosError;
import io.camlcase.javatezos.model.TezosErrorType;
import io.camlcase.javatezos.test.network.test.MockServerTest;
import io.camlcase.javatezos.test.util.CurrentThreadExecutor;
import io.camlcase.javatezos.test.util.Params;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Map;

/**
 * Integration tests for RPC GET Methods in {@link TezosNodeClient}
 */
public class TezosNodeClientGetTest extends MockServerTest {
    @Test
    public void testGetHeadOK() throws InterruptedException {
        callMockDispatcher();
        TezosCallback<Map<String, Object>> callback = new TezosCallback<Map<String, Object>>() {
            @Override
            public void onSuccess(@Nullable Map<String, Object> item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(6, item.size());
                Assert.assertTrue(item.containsKey("protocol"));
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("onFailure: "+error);
                Assert.fail("Should not call onFailure "+error);
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getHead(callback);
        countDownLatch.await();
    }

    @Test
    public void testGetHeadKO() throws InterruptedException {
        callErrorDispatcher();
        TezosCallback<Map<String, Object>> callback = new TezosCallback<Map<String, Object>>() {
            @Override
            public void onSuccess(@Nullable Map<String, Object> item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getHead(callback);
        countDownLatch.await();
    }

    @Test
    public void testGetHeadHashOK() throws InterruptedException {
        callMockDispatcher();
        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals("12345FakeHeadHash", item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getHeadHash(callback);
        countDownLatch.await();
    }

    @Test
    public void testGetHeadHashKO() throws InterruptedException {
        callErrorDispatcher();
        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getHeadHash(callback);
        countDownLatch.await();
    }

    @Test
    public void testGetBalanceOK() throws InterruptedException {
        callMockDispatcher();
        TezosCallback<Tez> callback = new TezosCallback<Tez>() {
            @Override
            public void onSuccess(@Nullable Tez item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(BigInteger.valueOf(272), item.getIntegerAmount());
                Assert.assertEquals(BigInteger.valueOf(957810), item.getDecimalAmount());
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getBalance("123address", callback);
        countDownLatch.await();
    }

    @Test
    public void testGetBalanceKO() throws InterruptedException {
        callErrorDispatcher();
        TezosCallback<Tez> callback = new TezosCallback<Tez>() {
            @Override
            public void onSuccess(@Nullable Tez item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getBalance("123address", callback);
        countDownLatch.await();
    }

    @Test
    public void testGetDelegateOK() throws InterruptedException {
        callMockDispatcher();
        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(Params.fakeAddress, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getDelegate("OneFakeAddress", callback);
        countDownLatch.await();
    }

    @Test
    public void testGetDelegateKO() throws InterruptedException {
        callErrorDispatcher();
        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getDelegate("OneFakeAddress", callback);
        countDownLatch.await();
    }

    @Test
    public void testGetContractStorageOK() throws InterruptedException {
        callMockDispatcher();
        TezosCallback<Map<String, Object>> callback = new TezosCallback<Map<String, Object>>() {
            @Override
            public void onSuccess(@Nullable Map<String, Object> item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(1, item.size());
                Assert.assertTrue(item.containsKey("string"));
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getContractStorage("KT1Address", callback);
        countDownLatch.await();
    }

    @Test
    public void testGetContractStorageKO() throws InterruptedException {
        callErrorDispatcher();
        TezosCallback<Map<String, Object>> callback = new TezosCallback<Map<String, Object>>() {
            @Override
            public void onSuccess(@Nullable Map<String, Object> item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getContractStorage("KT1Address", callback);
        countDownLatch.await();
    }

    @Test
    public void testGetBigMapValueOK() throws InterruptedException {
        callMockDispatcher();
        TezosCallback<Map<String, Object>> callback = new TezosCallback<Map<String, Object>>() {
            @Override
            public void onSuccess(@Nullable Map<String, Object> item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(2, item.size());
                Assert.assertTrue(item.containsKey("prim"));
                Assert.assertTrue(item.containsKey("args"));
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        client.getBigMapValue("KT1Address", new StringMichelsonParameter("tz1Address", null), MichelsonComparable.ADDRESS, callback);
        countDownLatch.await();
    }
}
