/**
 * # Released under MIT License
 * <p>
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.integration;

import io.camlcase.javatezos.NetworkClient;
import io.camlcase.javatezos.TezosNodeClient;
import io.camlcase.javatezos.data.RPC;
import io.camlcase.javatezos.model.TezosCallback;
import io.camlcase.javatezos.test.util.CurrentThreadExecutor;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

/**
 * Creation tests for {@link TezosNodeClient}
 */
public class TezosNodeClientTest {

    @Test
    public void testCreationDefaultContructor() {
        TezosNodeClient client = new TezosNodeClient();
        Assert.assertNotNull(client);
    }

    @Test
    public void testCreationPrimaryContructor() {
        NetworkClient javaImplementationMockClient = new NetworkClient() {
            public <T> void send(@NotNull RPC<T> rpc, @NotNull final TezosCallback<T> callback) {
                // Nothing to do
            }

            @NotNull
            public <T> CompletableFuture<T> send(@NotNull RPC<T> rpc) {
                CompletableFuture<T> future = new CompletableFuture<>();
                future.complete(null);
                return future;
            }
        };

        TezosNodeClient client = new TezosNodeClient(javaImplementationMockClient, new CurrentThreadExecutor());
        Assert.assertNotNull(client);
    }

}
