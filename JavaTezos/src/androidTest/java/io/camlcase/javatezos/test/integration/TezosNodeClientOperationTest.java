/**
 * # Released under MIT License
 * <p>
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.integration;

import io.camlcase.javatezos.TezosNodeClient;
import io.camlcase.javatezos.michelson.*;
import io.camlcase.javatezos.model.*;
import io.camlcase.javatezos.operation.OperationFeesPolicy;
import io.camlcase.javatezos.test.network.test.MockServerTest;
import io.camlcase.javatezos.test.util.CurrentThreadExecutor;
import io.camlcase.javatezos.test.util.Params;
import io.camlcase.javatezos.test.util.RPCConstants;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Integration tests for RPC POST Methods in {@link TezosNodeClient}
 */
public class TezosNodeClientOperationTest extends MockServerTest {

    private static List<String> MNEMONIC = new ArrayList<>(Arrays.asList(
            "soccer",
            "click",
            "number",
            "muscle",
            "police",
            "corn",
            "couch",
            "bitter",
            "gorilla",
            "camp",
            "camera",
            "shove",
            "expire",
            "praise",
            "pill"));

    @Test
    public void testSendTransactionOK() throws InterruptedException {
        callMockDispatcher();
        Wallet wallet = Wallet.Companion.invoke(MNEMONIC, "");
        Tez tez = Tez.Companion.invoke(1.0);
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(RPCConstants.INJECT_RESULT, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.send(tez, Params.fakeAddress, wallet.getAddress(), wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }

    @Test
    public void testSendTransactionKO() throws InterruptedException {
        callErrorDispatcher();
        Wallet wallet = Wallet.Companion.invoke(MNEMONIC, "");
        Tez tez = Tez.Companion.invoke(1.0);
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.send(tez, Params.fakeAddress, wallet.getAddress(), wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }


    @Test
    public void testDelegateOK() throws InterruptedException {
        callMockDispatcher();
        Wallet wallet = Wallet.Companion.invoke(MNEMONIC, "");
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(RPCConstants.INJECT_RESULT, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.delegate(wallet.getAddress(), Params.fakeAddress, wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }

    @Test
    public void testDelegateKO() throws InterruptedException {
        callErrorDispatcher();
        Wallet wallet = Wallet.Companion.invoke(MNEMONIC, "");
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.delegate(wallet.getAddress(), Params.fakeAddress, wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }

    @Test
    public void testCallSmartContractOK() throws InterruptedException {
        callMockDispatcher();
        Wallet wallet = Wallet.Companion.invoke(MNEMONIC, "");

        MichelsonParameter michelson = new PairMichelsonParameter(
                new StringMichelsonParameter("test", null),
                new RightMichelsonParameter(
                        new IntegerMichelsonParameter(10, null),
                        null
                ),
                null
        );
        TezosNodeClient client = new TezosNodeClient(getMockClient(), new CurrentThreadExecutor());
        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(RPCConstants.INJECT_RESULT, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.call(wallet.getAddress(), "KT1TEST", Tez.Companion.getZero(), michelson, wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }
}
