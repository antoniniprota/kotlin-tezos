/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.operation

import io.camlcase.javatezos.crypto.PublicKey
import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.crypto.SigningService
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.javatezos.test.util.Params.Companion.fakeOperationPayload
import io.camlcase.javatezos.test.util.RPCConstants.Companion.FORGE_RESULT
import io.camlcase.javatezos.test.util.fakeSignature
import io.mockk.impl.annotations.SpyK
import io.mockk.spyk
import io.mockk.verify
import org.junit.Assert
import org.junit.Test

class SigningServiceTest {
    @SpyK
    val metadata = fakeMetadata
    
    @SpyK
    private val signature: SignatureProvider = spyk(object : SignatureProvider {
        override val publicKey: PublicKey
            get() = PublicKey(ByteArray(8))

        override fun sign(hex: String): ByteArray {
            return ByteArray(8)
        }
    })

    @Test
    fun testSignServiceOK() {
        val response = SigningService.sign(fakeOperationPayload, metadata, FORGE_RESULT, signature)
        Assert.assertNotNull(response)
        Assert.assertEquals(
            fakeOperationPayload,
            response.signedProtocolOperationPayload.signedOperationPayload.operationPayload
        )
        verify(exactly = 1) {
            signature.sign(FORGE_RESULT)
            metadata.protocol
            metadata.chainId
        }
    }

    @Test(expected = TezosError::class)
    fun testSignServiceForgeIsEmpty() {
        SigningService.sign(fakeOperationPayload, metadata, "", fakeSignature)
        verify(exactly = 1) {
            signature.sign(FORGE_RESULT)
        }
        verify(exactly = 0) {
            metadata.protocol
            metadata.chainId
        }
    }

    @Test(expected = TezosError::class)
    fun testSignServiceForgeIsNull() {
        SigningService.sign(fakeOperationPayload, metadata, null, fakeSignature)
        verify(exactly = 1) {
            signature.sign(FORGE_RESULT)
        }
        verify(exactly = 0) {
            metadata.protocol
            metadata.chainId
        }
    }

    @Test(expected = TezosError::class)
    fun testSignServiceSignatureIsNull() {
        val fakeSignature = object : SignatureProvider {
            override val publicKey: PublicKey
                get() = PublicKey(ByteArray(8))

            override fun sign(hex: String): ByteArray? {
                return null
            }
        }
        SigningService.sign(fakeOperationPayload, metadata, null, fakeSignature)
        verify(exactly = 1) {
            signature.sign(FORGE_RESULT)
        }
        verify(exactly = 0) {
            metadata.protocol
            metadata.chainId
        }
    }

    companion object {
        private const val EXPECTED_SIGNED: String =
            "8a700ab69b834bc5aa3b892f809bf2dc2cfdb61700fd8cd3197884954aec76e56c00b9232920ab58cabc9fa969b5cae8a87bf8827fe79d0a80d80880ea30e0d4030100007659a8eb542f8e3cfb24259dc6650dec5e9a1a33000000000000000000"
    }
}
