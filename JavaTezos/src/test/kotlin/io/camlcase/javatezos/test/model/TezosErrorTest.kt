/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.model

import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.TezosErrorType
import org.junit.Assert
import org.junit.Test

class TezosErrorTest {
    @Test
    fun testErrorCreationWithNoException() {
        val error = TezosError(TezosErrorType.UNKNOWN)
        Assert.assertNull(error.message)
        Assert.assertNull(error.cause)
        Assert.assertNull(error.rpcErrors)
        Assert.assertNotNull(error.type)
        Assert.assertEquals(TezosErrorType.UNKNOWN, error.type)
    }

    @Test
    fun testErrorCreationWithEmptyException() {
        val error = TezosError(TezosErrorType.UNKNOWN, exception = IllegalStateException())
        Assert.assertNull(error.message)
        Assert.assertNull(error.cause)
        Assert.assertNull(error.rpcErrors)
        Assert.assertNotNull(error.type)
        Assert.assertEquals(TezosErrorType.UNKNOWN, error.type)
    }

    @Test
    fun testErrorCreationWithMessageException() {
        val error = TezosError(TezosErrorType.UNKNOWN, exception = IllegalStateException("Something happened"))
        Assert.assertNotNull(error.message)
        Assert.assertNull(error.cause)
        Assert.assertNull(error.rpcErrors)
        Assert.assertNotNull(error.type)
        Assert.assertEquals(TezosErrorType.UNKNOWN, error.type)
    }
}
