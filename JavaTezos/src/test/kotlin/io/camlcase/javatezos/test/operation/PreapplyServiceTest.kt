/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.test.operation

import io.camlcase.javatezos.model.operation.payload.SignedOperationPayload
import io.camlcase.javatezos.model.operation.payload.SignedProtocolOperationPayload
import io.camlcase.javatezos.operation.PreapplyService
import io.camlcase.javatezos.test.network.test.MockServerTest
import io.camlcase.javatezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.javatezos.test.util.Params.Companion.fakeOperationPayload
import io.camlcase.javatezos.test.util.testFailure
import org.junit.Assert
import org.junit.Test

class PreapplyServiceTest : MockServerTest() {

    @Test
    fun testCallOK() {
        callMockDispatcher()
        val service = PreapplyService(mockClient)
        val result = service.preapply(payload, fakeMetadata)
            .join()

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.isEmpty())
    }

    @Test
    fun testCallKO() {
        callErrorDispatcher()
        val service = PreapplyService(mockClient)
        service.preapply(payload, fakeMetadata)
            .testFailure(countDownLatch)
        countDownLatch.await()
    }

    companion object {
        private val payload = SignedProtocolOperationPayload(
            SignedOperationPayload(fakeOperationPayload, "fakeChainId", "fakeSignature"),
            "fakeProtocol"
        )
    }
}
