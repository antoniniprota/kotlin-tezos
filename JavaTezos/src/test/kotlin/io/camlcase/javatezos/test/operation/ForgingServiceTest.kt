/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.operation

import io.camlcase.javatezos.operation.ForgingPolicy
import io.camlcase.javatezos.operation.ForgingService
import io.camlcase.javatezos.test.network.test.MockServerTest
import io.camlcase.javatezos.test.util.*
import io.camlcase.javatezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.javatezos.test.util.Params.Companion.fakeOperationPayload
import io.camlcase.javatezos.test.util.Params.Companion.fakeTransactionOperation
import io.camlcase.javatezos.test.util.RPCConstants.Companion.FORGE_RESULT
import io.mockk.spyk
import io.mockk.verify
import org.junit.Assert
import org.junit.Test

/**
 * Integration tests for [ForgingService]
 */
class ForgingServiceTest : MockServerTest() {
    private val testExecutor = CurrentThreadExecutor()

    private val verification: (String?) -> Unit = {
        Assert.assertNotNull(it)
        Assert.assertEquals(FORGE_RESULT, it)
    }

    @Test
    fun testRemotePolicyOK() {
        callMockDispatcher()
        val service = spyk(ForgingService(mockClient, testExecutor))
        val metadata = spyk(fakeMetadata)
        val result = service.forge(
            ForgingPolicy.REMOTE,
            "tz1_Fake",
            fakeTransactionOperation,
            metadata,
            spyk(fakeSignature)
        ).join()

        verification(result)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, any(), metadata)
            fakeMetadata.branch
            fakeMetadata.key
            fakeMetadata.counter
            fakeMetadata.protocol
            fakeSignature.publicKey
        }
    }

    @Test
    fun testRemotePolicyKO() {
        callErrorDispatcher()
        val service = spyk(ForgingService(mockClient, testExecutor))

        val metadata = spyk(fakeMetadata)
        service.forge(
            ForgingPolicy.REMOTE,
            "tz1_Fake",
            fakeTransactionOperation,
            metadata,
            spyk(fakeSignature)
        ).testFailure(countDownLatch)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, any(), metadata)
        }

        countDownLatch.await()
    }


    @Test
    fun testRemotePolicyWithPayloadAndCallbackOK() {
        callMockDispatcher()
        val service = spyk(ForgingService(mockClient, testExecutor))

        service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata,
            TestSuccessCallback(countDownLatch, verification)
        )

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
        }
        countDownLatch.await()
    }

    @Test
    fun testRemotePolicyWithPayloadAndCallbackKO() {
        callErrorDispatcher()
        val service = spyk(ForgingService(mockClient, testExecutor))

        service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata,
            TestFailureCallback(countDownLatch)
        )

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
        }
        countDownLatch.await()
    }


    @Test
    fun testRemotePolicyWithPayloadOK() {
        callMockDispatcher()
        val service = spyk(ForgingService(mockClient, testExecutor))

        val result = service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata
        ).join()

        verification(result)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
        }
    }

    @Test
    fun testRemotePolicyWithPayloadKO() {
        callErrorDispatcher()
        val service = spyk(ForgingService(mockClient, testExecutor))

        service.forge(
            ForgingPolicy.REMOTE,
            fakeOperationPayload,
            fakeMetadata
        ).testFailure(countDownLatch)

        verify(exactly = 1) {
            service.forge(ForgingPolicy.REMOTE, fakeOperationPayload, fakeMetadata)
        }
        countDownLatch.await()
    }
}
