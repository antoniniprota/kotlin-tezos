/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.model

import io.camlcase.javatezos.model.Tez
import io.camlcase.javatezos.model.TezosError
import org.junit.Assert
import org.junit.Test

/**
 * Test suite for [Tez]
 */
class TezTest {

    @Test
    fun testTezCreationWith3Decimal() {
        val tez = Tez(123.456)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456000", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWith6Decimal() {
        val tez = Tez(123.000456)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123000456", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWith6Decimal2() {
        val tez = Tez(123.456789)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456789.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456789", tez.stringRepresentation)

    }

    @Test
    fun testTezCreationLostDecimal() {
        val tez = Tez(123.000456789)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123000456", tez.stringRepresentation)
    }

    @Test
    fun testTezCreation8LongInteger() {
        val tez = Tez(12345678.1)
        Assert.assertEquals(12345678.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(100000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("12345678100000", tez.stringRepresentation)
    }

    @Test(expected = TezosError::class)
    fun testTezCreationWithEmptyString() {
        Tez("")
    }

    @Test(expected = TezosError::class)
    fun testTezCreationWithInvalidString() {
        Tez("1.90")
    }

    @Test
    fun testTezCreationWithStringSmallNumber() {
        val tez = Tez("10") // 0.000010 XTC
        Assert.assertEquals(0.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(10.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("10", tez.stringRepresentation)

    }

    @Test
    fun testTezCreationWithStringWholeNumber() {
        val tez = Tez("3000000") // 3 XTC
        Assert.assertEquals(3.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(0.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("3000000", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWithStringWithDecimal() {
        val tez = Tez("3500000") // 3.5 XTC
        Assert.assertEquals(3.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(500000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("3500000", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWithStringWithVeryBigAmount() {
        val tez = Tez("1000000000000000000000") // 10^ XTC
        Assert.assertEquals(1000000000000000.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(0.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("1000000000000000000000", tez.stringRepresentation)
    }
}
