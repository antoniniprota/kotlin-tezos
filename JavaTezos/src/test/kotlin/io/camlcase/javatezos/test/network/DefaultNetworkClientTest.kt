/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */

package io.camlcase.javatezos.test.network

/**
 * Test suite for [DefaultNetworkClient]
 */
class DefaultNetworkClientTest
