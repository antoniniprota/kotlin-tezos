/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.ext

import org.json.JSONArray
import org.json.JSONObject

fun String.removeWhitespaces() = this.replace("\\s".toRegex(), "")

internal fun String?.equalsToWildcard(stringWithWildcard: String): Boolean {
    val regex = stringWithWildcard.replace("?", ".?")
        .replace("*", ".*?")
    return this?.matches(Regex(regex)) == true
}

/**
 * Returns a JSON encoded string representation of a given string.
 */
internal fun String?.toJson(): String? {
    return this?.let { "\"" + it + "\"" }
}

/**
 * Converts a map of content to a JSONArray of one element.
 */
internal fun Map<String, Any>?.toJsonArray(): String? {
    return try {
        val jsonObject = JSONObject(this)
        val array = JSONArray()
        array.put(jsonObject)
        return array.toString()
    } catch (e: Exception) {
        null
    }

}

internal fun Map<String, Any>?.toJson(): String? {
    return try {
        val jsonObject = JSONObject(this)
        jsonObject.toString()
    } catch (e: Exception) {
        null
    }
}
