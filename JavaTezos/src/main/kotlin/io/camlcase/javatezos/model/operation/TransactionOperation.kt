/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.model.operation

import io.camlcase.javatezos.michelson.MichelsonParameter
import io.camlcase.javatezos.model.Address
import io.camlcase.javatezos.model.Tez
import io.camlcase.javatezos.model.operation.fees.OperationFees

/**
 * An operation to transact XTZ between addresses.
 *
 * @param parameter An optional parameter to include in the transaction if the call is being made to a smart contract.
 */
internal data class TransactionOperation(
    val amount: Tez,
    override val source: Address,
    val destination: Address,
    override val fees: OperationFees,
    val parameter: MichelsonParameter? = null
) : BaseOperation {
    override val type: OperationType
        get() = OperationType.TRANSACTION

    override val payload: MutableMap<String, Any>
        get() {
            val payload = super.payload
            payload[PAYLOAD_ARG_AMOUNT] = amount.stringRepresentation
            payload[PAYLOAD_ARG_DESTINATION] = destination

            parameter?.let {
                val parameters = HashMap<String, Any>()
                parameters[PAYLOAD_ARG_SMART_CONTRACT_ENTRYPOINT] = PAYLOAD_VALUE_SMART_CONTRACT_ENTRYPOINT_DEFAULT
                parameters[PAYLOAD_ARG_SMART_CONTRACT_VALUE] = it.payload
                payload[PAYLOAD_ARG_SMART_CONTRACT_PARAMETERS] = parameters
            }
            return payload
        }

    override fun copy(newFees: OperationFees): Operation {
        return TransactionOperation(amount, source, destination, newFees)
    }

    companion object {
        const val PAYLOAD_ARG_AMOUNT = "amount"
        const val PAYLOAD_ARG_DESTINATION = "destination"

        const val PAYLOAD_ARG_SMART_CONTRACT_PARAMETERS = "parameters"
        const val PAYLOAD_ARG_SMART_CONTRACT_ENTRYPOINT = "entrypoint"
        const val PAYLOAD_ARG_SMART_CONTRACT_VALUE = "value"

        const val PAYLOAD_VALUE_SMART_CONTRACT_ENTRYPOINT_DEFAULT = "default"
    }
}
