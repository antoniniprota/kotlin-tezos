/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.model

import io.camlcase.javatezos.model.Tez.Companion.invoke
import java.math.BigDecimal
import java.math.BigInteger
import kotlin.math.pow

/**
 * Object representation of an amount of Tez.
 *
 * @see [invoke] for secondary constructors
 */
data class Tez(
    /**
     * Integer part of the amount
     * E.g.: An amount of 123.456 will have a [integerAmount] of 123
     */
    val integerAmount: BigInteger,
    /**
     * Decimal part of the amount
     * E.g.: An amount of 123.456 will have a [decimalAmount] of 456
     */
    val decimalAmount: BigInteger
) : Comparable<Tez> {

    /**
     * A representation of the given balance for use in RPC requests.
     */
    val stringRepresentation: String
        get() {
            var decimalString = decimalAmount.toString()
            while (decimalString.length < DECIMAL_COUNT) {
                decimalString = "0$decimalString"
            }
            val fullAmount = "$integerAmount$decimalString"
            return if (fullAmount.toBigInteger() == 0.toBigInteger()) {
                fullAmount
            } else {
                // Trim any leading zeroes by converting to an Int.
                fullAmount.replace(Regex("^0+"), "")
            }
        }

    /**
     * Sum of Tez
     */
    operator fun plus(other: Tez): Tez {
        var intSum = integerAmount + other.integerAmount
        var decimalSum = decimalAmount + other.decimalAmount

        val multiplier = BigDecimal(10.0.pow(6.0)).toBigInteger()
        val one = 1.toBigInteger()
        // Handle carry of decimals
        if (decimalSum / multiplier >= one) {
            intSum += one
            decimalSum -= multiplier
        }
        return Tez(intSum, decimalSum)
    }

    /**
     * Substraction of Tez
     */
    operator fun minus(other: Tez): Tez {
        var intSubstract = integerAmount - other.integerAmount
        var decimalSubstract = decimalAmount - other.decimalAmount

        val multiplier = BigDecimal(10.0.pow(6.0)).toBigInteger()
        // Handle carry of decimals
        if (decimalSubstract < 0.toBigInteger()) {
            intSubstract -= 1.toBigInteger()
            decimalSubstract += multiplier
        }
        return Tez(intSubstract, decimalSubstract)
    }

    override fun compareTo(other: Tez): Int {
        val right = BigInteger(stringRepresentation)
        val left = BigInteger(other.stringRepresentation)
        return right.compareTo(left)
    }

    companion object {
        const val DECIMAL_COUNT = 6
        val zero = Tez(0.toBigInteger(), 0.toBigInteger())

        /**
         * Initialize a new balance from a given decimal number.
         */
        operator fun invoke(balance: Double) = run {
            // 123.456 to 123
            val integerValue = BigDecimal.valueOf(balance).toBigInteger()
            val multiplier = 10.0.pow(6.0)

            // 123.456 to 123456
            val allToInteger = BigDecimal(balance * multiplier).toBigInteger()
            // 123 to 123000
            val integerPartWithPow: BigInteger = integerValue * BigDecimal(multiplier).toBigInteger()
            // 123456 - 123000 = 456
            val decimalValue = allToInteger - integerPartWithPow

            Tez(integerValue, decimalValue)
        }

        /**
         * Initialize a new balance from an RPC representation of a balance.
         * @throws TezosError if [balanceRepresentation] is not a valid numerical value
         */
        operator fun invoke(balanceRepresentation: String): Tez = run {
            val invalidNumber = TezosError(
                TezosErrorType.INTERNAL_ERROR,
                exception = IllegalArgumentException("[Tez] \"$balanceRepresentation\" is not a valid numerical value")
            )

            return if (balanceRepresentation.matches(Regex("[0-9]+")) && balanceRepresentation.isNotEmpty()) {
                var paddedBalance = balanceRepresentation
                while (paddedBalance.length < DECIMAL_COUNT) {
                    paddedBalance = "0$paddedBalance"
                }

                val separatorIndex = paddedBalance.length - DECIMAL_COUNT
                val integerRange = if (separatorIndex == 0) "0" else paddedBalance.substring(0, separatorIndex)
                val decimalRange = paddedBalance.substring(separatorIndex, paddedBalance.length)

                try {
                    Tez(BigInteger(integerRange), BigInteger(decimalRange))
                } catch (e: NumberFormatException) {
                    throw invalidNumber
                }
            } else {
                throw invalidNumber
            }
        }
    }
}
