/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.model.operation.payload

import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.data.Payload
import io.camlcase.javatezos.model.Address
import io.camlcase.javatezos.model.operation.Operation
import io.camlcase.javatezos.model.operation.OperationMetadata
import io.camlcase.javatezos.model.operation.OperationType
import io.camlcase.javatezos.model.operation.RevealOperation
import io.camlcase.javatezos.operation.fees.DefaultFeeEstimator

/**
 * A payload that can be forged into operation bytes.
 *
 * @param operations An array of dictionaries representing operations.
 * @param branch The hash of the head of the chain to apply the operation on.
 */
data class OperationPayload(
    val operations: Iterable<OperationWithCounter>,
    val branch: String
) : Payload {

    constructor(operations: Iterable<OperationWithCounter>, metadata: OperationMetadata) : this(
        operations,
        metadata.branch
    )

    override val payload: MutableMap<String, Any>
        get() {
            val operationsPayload: List<Map<String, Any>> = operations.map { it.payload }
            val map = HashMap<String, Any>()
            map["contents"] = operationsPayload
            map["branch"] = branch
            return map
        }

    companion object {

        /**
         * Constructor: Create an operation payload from the given inputs.
         */
        operator fun invoke(
            operations: Iterable<Operation>,
            source: Address,
            metadata: OperationMetadata,
            signatureProvider: SignatureProvider
        ) = run {
            val checkedOperations = checkOperations(operations, source, metadata, signatureProvider)
            val operationsWithCounter = parseOperations(checkedOperations, metadata)
            OperationPayload(operationsWithCounter, metadata)
        }

        /**
         * Determine if the address performing the operations has been revealed. If it has not been, check if any of
         * the operations to perform requires the address to be revealed. If so, prepend a reveal operation to
         * the operations to perform.
         */
        private fun checkOperations(
            operations: Iterable<Operation>,
            source: Address,
            metadata: OperationMetadata,
            signatureProvider: SignatureProvider
        ): Iterable<Operation> {
            val list = ArrayList<Operation>()
            val needsRevealOperation =
                metadata.key.isNullOrBlank() && operations.firstOrNull { it.requiresReveal } != null
            if (needsRevealOperation) {
                val revealOperation = RevealOperation(
                    source,
                    signatureProvider.publicKey,
                    DefaultFeeEstimator.calculateFees(metadata.tezosProtocol, OperationType.REVEAL)
                )
                list.add(revealOperation)
            }
            list.addAll(operations)
            return list
        }

        /**
         * Process all operations to have increasing counters and place them in the contents array.
         */
        private fun parseOperations(
            operations: Iterable<Operation>,
            metadata: OperationMetadata
        ): Iterable<OperationWithCounter> {
            val list = ArrayList<OperationWithCounter>()
            var nextCounter = metadata.counter + 1
            operations.forEach {
                list.add(OperationWithCounter(it, nextCounter))
                nextCounter += 1
            }

            return list
        }
    }
}
