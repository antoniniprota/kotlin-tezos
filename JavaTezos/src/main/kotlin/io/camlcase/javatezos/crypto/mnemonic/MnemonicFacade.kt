/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.crypto.mnemonic

import io.camlcase.javatezos.crypto.toHexString
import io.camlcase.javatezos.crypto.toSeed
import io.camlcase.javatezos.model.TezosError
import kotlin.random.Random

/**
 * Wrapper for BIP39 tooling in Java/Kotlin. Supports English mnemonics only (for now).
 */
object MnemonicFacade {

    private val mnemonicCode: MnemonicCode by lazy {
        MnemonicCode()
    }

    /**
     * Generate a mnemonic of the given strength in english.
     * Process is heavyweight, recommended to launch this in a computation thread.
     * @param strength This must be a multiple of 32.
     * @return list of mnemonics, null if strenght is not valid
     */
    fun generateMnemonic(strength: Int): List<String>? {
        val array = ByteArray(strength)
        Random.Default.nextBytes(array)
        return try {
            mnemonicCode.toMnemonic(array)
        } catch (e: TezosError) {
            null
        }
    }

    /**
     * Validate that the given list of words is a valid mnemonic.
     */
    fun validate(mnemonic: List<String>): Boolean {
        fun String.sanitize(): String {
            return this.toLowerCase()
                .trim()
                .replace("\n", "")
        }

        if (mnemonic.isEmpty()) {
            return false
        }

        if (ENGLISH_MNEMONICS.contains(mnemonic[0].sanitize())) {
            mnemonic.forEach {
                if (!ENGLISH_MNEMONICS.contains(it.sanitize())) {
                    return false
                }
            }
            return true
        }
        return false
    }

    /**
     * Generate a seed from a given mnemonic.
     * @param mnemonic A BIP39 mnemonic phrase.
     * @param passphrase An optional passphrase used for encryption.
     */
    fun seed(mnemonic: List<String>, passphrase: String = ""): ByteArray {
        return mnemonicCode.toSeed(mnemonic, passphrase)
    }

    /**
     * Generate a seed string from a given mnemonic.
     * @param mnemonic A BIP39 mnemonic phrase.
     * @param passphrase An optional passphrase used for encryption.
     * @return 64 byte of seed hex string
     */
    fun seedString(mnemonic: List<String>, passphrase: String = ""): String? {
        return if (validate(mnemonic)) {
            val seed: ByteArray = seed(mnemonic, passphrase)
            seed.toHexString().substring(0, 64)
        } else {
            null
        }
    }
}

