/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.operation

import io.camlcase.javatezos.NetworkClient
import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.data.RunOperationRPC
import io.camlcase.javatezos.ext.toCallback
import io.camlcase.javatezos.model.Address
import io.camlcase.javatezos.model.TezosCallback
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.TezosErrorType
import io.camlcase.javatezos.model.operation.Operation
import io.camlcase.javatezos.model.operation.OperationMetadata
import io.camlcase.javatezos.model.operation.SimulationResponse
import io.camlcase.javatezos.model.operation.payload.OperationPayload
import io.camlcase.javatezos.model.operation.payload.SignedOperationPayload
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Connects to the RPC to simulate an operation
 */
class SimulationService(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) {

    /**
     * Simulate the given operation.
     *
     * @param callback To return the [SimulationResponse] asynchronously or [TezosError]
     */
    fun simulate(
        operation: Operation,
        from: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider,
        callback: TezosCallback<SimulationResponse>
    ) {
        simulate(operation, from, metadata, signatureProvider)
            .toCallback(callback)
    }

    /**
     * Simulate the given operation.
     *
     * @return Completable of a [SimulationResponse]
     * @throws [TezosError]
     */
    fun simulate(
        operation: Operation,
        from: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<SimulationResponse> {
        return Future(executorService) {
            createPayload(operation, from, metadata, signatureProvider)
        }.flatMap { simulateOperation(it) }
            .map {
                if (it == null) {
                    throw TezosError(
                        TezosErrorType.INTERNAL_ERROR,
                        exception = IllegalArgumentException("[SimulationService] Failed to simulate for operation { ${operation.type} }")
                    )
                }
                it!!
            }
    }

    private fun createPayload(
        operation: Operation,
        from: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): SignedOperationPayload {
        val operationPayload =
            OperationPayload(listOf(operation).asIterable(), from, metadata, signatureProvider)
        return SignedOperationPayload(operationPayload, metadata.chainId, simulationSignature)
    }

    private fun simulateOperation(signedOperationPayload: SignedOperationPayload): CompletableFuture<SimulationResponse?> {
        return networkClient.send(RunOperationRPC(signedOperationPayload))
    }

    companion object {
        private const val SIGNATURE_LENGHT = 64
        private val simulationSignature: ByteArray = ByteArray(SIGNATURE_LENGHT)
    }
}
