/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.operation.fees

import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.ext.badArgumentsError
import io.camlcase.javatezos.model.*
import io.camlcase.javatezos.model.operation.*
import io.camlcase.javatezos.model.operation.fees.OperationFees
import io.camlcase.javatezos.operation.ForgingPolicy
import io.camlcase.javatezos.operation.ForgingService
import io.camlcase.javatezos.operation.OperationFactory
import io.camlcase.javatezos.operation.SimulationService
import io.camlcase.javatezos.operation.fees.FeeConstants.FEE_PER_GAS_UNIT
import io.camlcase.javatezos.operation.fees.FeeConstants.FEE_PER_STORAGE_BYTE
import io.camlcase.javatezos.operation.fees.FeeConstants.MINIMAL_FEE
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.toCompletableFuture
import io.github.vjames19.futures.jdk8.zip
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * A class which can estimate fee, gas and storage limits for an operation.
 */
class FeeEstimator(
    private val executorService: ExecutorService,
    private val simulationService: SimulationService,
    private val forgingService: ForgingService
) {

    /**
     * Estimate OperationFees for the given inputs.
     *
     * @throws [TezosError] If simulation error
     */
    fun calculateFees(
        type: OperationType,
        params: OperationParams,
        address: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<OperationFees> {
        OperationFactory.createOperation(type, params, OperationFees.simulationFees)?.let { operation ->
            return calculateFees(operation, address, metadata, signatureProvider)
        } ?: return badArgumentsError("[FeeEstimator] Invalid parameters: { $type, $params } to create an operation")
            .toCompletableFuture()
    }

    /**
     * 1) Simulate the operation to determine gas and storage limits.
     * 2) Calculate initial gas with those limits
     * 3) Forge operation to retrieve initial storage fees
     * 4) Loop and forge until storage fees are above the required storage fee
     *
     * @throws [TezosError] If error in simulation or forge operations
     */
    private fun calculateFees(
        operation: Operation,
        address: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<OperationFees> {
        return simulateFees(operation, address, metadata, signatureProvider)
            .zip(
                getFeesFromForge(operation, address, metadata, signatureProvider),
                executorService
            ) { simulatedFees, forgedTez ->
                calculateFees(operation, address, metadata, signatureProvider, simulatedFees, forgedTez)
            }
    }

    /**
     * @throws TezosError If Simulation service returns a null OperationFees
     */
    private fun simulateFees(
        operation: Operation,
        address: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<OperationFees> {
        return simulationService.simulate(operation, address, metadata, signatureProvider)
            .map(executorService) {
                calculateInitialFees(it)
            }
    }

    /**
     * @throws TezosError If empty forge response
     */
    private fun getFeesFromForge(
        operation: Operation,
        address: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<Tez> {
        return forgingService.forge(ForgingPolicy.REMOTE, address, operation, metadata, signatureProvider)
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("[FeeEstimator] Invalid parameters: { ${operation.type}, $address } to forge an operation")
                }
                // Get the fee required for the given serialized operation.
                val nanoTez = it.length * FEE_PER_STORAGE_BYTE
                nanoTez.toTez()
            }
    }

    private fun calculateInitialFees(response: SimulationResponse): OperationFees {
        val gasLimit = response.consumedGas + SafetyMargin.GAS
        val storageLimit = response.consumedStorage + SafetyMargin.STORAGE

        // Start with a minimum fee equal to the minimum fee or the fee required by the gas.
        val initialFee = getInitialFee(gasLimit)
        return OperationFees(initialFee, gasLimit, storageLimit)
    }

    private fun getInitialFee(gasLimit: Int): Tez {
        val gasFee = feeForGas(gasLimit)
        val minimumFee = MINIMAL_FEE.toTez()
        return minimumFee + gasFee
    }

    /**
     * Calculate the fee required to fulfill the given gas limit.
     */
    private fun feeForGas(gas: Int): Tez {
        val nanoTez = gas * FEE_PER_GAS_UNIT
        return nanoTez.toTez()
    }

    /**
     * Loops until the storage fee for the operation is above the required storage fee.
     * This method calls [getFeesFromForge] synchronously, ensure it is inside a computation thread.
     *
     * @throws TezosError If Forge service fails
     */
    private fun calculateFees(
        operation: Operation,
        address: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider,
        simulatedFees: OperationFees,
        forgedStorageFee: Tez
    ): OperationFees {
        var requiredStorageFee = forgedStorageFee
        val initialFee = simulatedFees.fee
        var operationFees = simulatedFees
        val updatedOperation = operation.copy(operationFees)

        while ((operationFees.fee - initialFee) < requiredStorageFee) {
            val storageFee = operationFees.fee - initialFee
            val feeDifference = requiredStorageFee - storageFee
            val newFee: Tez = operationFees.fee + feeDifference
            operationFees = OperationFees(newFee, updatedOperation.fees.gasLimit, updatedOperation.fees.storageLimit)

            // Calculate a new required storage fee, based on the updated fees.
            requiredStorageFee = getFeesFromForge(
                updatedOperation.copy(operationFees),
                address, metadata, signatureProvider
            ).join()
        }

        val calculatedFee = operationFees.fee + SafetyMargin.FEE
        return OperationFees(calculatedFee, updatedOperation.fees.gasLimit, updatedOperation.fees.storageLimit)
    }
}

/**
 * Safety margins that will be added to fee estimations.
 */
object SafetyMargin {
    const val GAS = 100
    const val STORAGE = 257
    val FEE = Tez(0.000_100)
}

/**
 * Maximum values for limits in OperationFees.
 */
object Maximums {
    const val GAS = 800_000
    const val STORAGE = 60_000
}

/**
 * Constants that are used in fee calculations.
 */
object FeeConstants {
    const val MINIMAL_FEE: NanoTez = 100_000
    const val FEE_PER_GAS_UNIT: NanoTez = 100
    const val FEE_PER_STORAGE_BYTE: NanoTez = 1_000
}
