/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.operation

import io.camlcase.javatezos.NetworkClient
import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.data.ForgeOperationRPC
import io.camlcase.javatezos.ext.wrap
import io.camlcase.javatezos.model.Address
import io.camlcase.javatezos.model.TezosCallback
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.operation.Operation
import io.camlcase.javatezos.model.operation.OperationMetadata
import io.camlcase.javatezos.model.operation.SimulationResponse
import io.camlcase.javatezos.model.operation.payload.OperationPayload
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.onComplete
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * A service which manages forging of operations given a [ForgingPolicy]. Right now it only supports remote option.
 */
class ForgingService(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) {
    /**
     * Forge the given operation.
     * @return Completable with a String representing the result of the forge.
     */
    fun forge(
        policy: ForgingPolicy,
        address: Address,
        operation: Operation,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<String?> {
        return Future(executorService) {
            createPayload(operation, address, metadata, signatureProvider)
        }.flatMap { forge(policy, it, metadata) }
    }


    /**
     * Forge the given operation payload.
     * @param callback To return the [SimulationResponse] asynchronously or [TezosError]
     */
    fun forge(
        policy: ForgingPolicy,
        payload: OperationPayload,
        metadata: OperationMetadata,
        callback: TezosCallback<String>
    ) {
        forge(policy, payload, metadata)
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Forge the given operation payload.
     * @return Completable with a String representing the result of the forge.
     */
    fun forge(
        policy: ForgingPolicy,
        payload: OperationPayload,
        metadata: OperationMetadata
    ): CompletableFuture<String?> {
        // TODO Use ForgingPolicy to branch in different options
        return remoteForge(payload, metadata)

    }

    private fun createPayload(
        operation: Operation,
        from: Address,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): OperationPayload {
        return OperationPayload(listOf(operation).asIterable(), from, metadata, signatureProvider)
    }

    /**
     * Forge the given operation remotely on a node.
     */
    private fun remoteForge(payload: OperationPayload, metadata: OperationMetadata): CompletableFuture<String?> {
        return networkClient.send(ForgeOperationRPC(metadata.branch, payload))
    }
}

/**
 * An enum defining policies used to forge operations.
 */
enum class ForgingPolicy {
    /**
     * Always forge operations remotely on the node.
     */
    REMOTE
}
