/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.network.test

import io.camlcase.javatezos.data.InjectOperationRPC.Companion.RPC_PATH_INJECT
import io.camlcase.javatezos.data.RPC_GET_HEAD_HASH_PATH
import io.camlcase.javatezos.data.RPC_GET_HEAD_PATH
import io.camlcase.javatezos.data.RunOperationRPC.Companion.RPC_PATH_RUN_OPERATION
import io.camlcase.javatezos.ext.equalsToWildcard
import io.camlcase.javatezos.test.util.Params.Companion.fakeAddress
import io.camlcase.javatezos.test.util.RPCConstants.Companion.BIG_MAP_RESULT
import io.camlcase.javatezos.test.util.RPCConstants.Companion.FORGE_RESULT
import io.camlcase.javatezos.test.util.RPCConstants.Companion.INJECT_RESULT
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_FORGE_OPERATION
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_GET_BALANCE
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_GET_BIG_MAP_VALUE
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_GET_CONTRACT_STORAGE
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_GET_COUNTER
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_GET_DELEGATE
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_GET_MANAGER_KEY
import io.camlcase.javatezos.test.util.RPCConstants.Companion.RPC_PREAPPLY_OPERATION
import io.camlcase.javatezos.test.util.RPCConstants.Companion.STORAGE_RESULT
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

/**
 * Provides mock responses for a test server
 */
class MockDispatcher {

    fun getDispatcher(): RequestDispatcher {
        return RequestDispatcher()
    }

    fun getErrorDispatcher(): ErrorDispatcher {
        return ErrorDispatcher()
    }

    /**
     * Return ok response from mock server
     */
    inner class RequestDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            println("[!] MOCKWEBSERVER Request: $request")
            return when {
                request.path?.equals(RPC_GET_HEAD_PATH) == true ->
                    validResponse().setBody(getStringFromFile("get_head.json"))
                request.path?.equals(RPC_GET_HEAD_HASH_PATH) == true ->
                    validResponse().setBody("12345FakeHeadHash")
                request.path?.equalsToWildcard(RPC_GET_BALANCE) == true -> validResponse().setBody("272957810")
                request.path?.equalsToWildcard(RPC_GET_COUNTER) == true -> validResponse().setBody("12345")
                request.path?.equalsToWildcard(RPC_GET_MANAGER_KEY) == true -> validResponse().setBody("edpkuteCZgeYj1SARYZx2CWiNbLtpToR8a4f54hNqzVnHTqpAJoUZK")
                request.path?.equals(RPC_PATH_RUN_OPERATION) == true ->
                    validResponse().setBody(getStringFromFile("run_operation_simulation.json"))
                request.path?.equalsToWildcard(RPC_FORGE_OPERATION) == true -> validResponse().setBody(FORGE_RESULT)
                request.path?.equalsToWildcard(RPC_PREAPPLY_OPERATION) == true -> validResponse().setBody(
                    getStringFromFile("preapply.json")
                )
                request.path?.equals(RPC_PATH_INJECT) == true ->
                    validResponse().setBody(INJECT_RESULT)
                request.path?.equalsToWildcard(RPC_GET_DELEGATE) == true ->
                    validResponse().setBody(fakeAddress)
                request.path?.equalsToWildcard(RPC_GET_CONTRACT_STORAGE) == true ->
                    validResponse().setBody(STORAGE_RESULT)
                request.path?.equalsToWildcard(RPC_GET_BIG_MAP_VALUE) == true ->
                   validResponse().setBody(BIG_MAP_RESULT)
                else -> error404()
            }
        }
    }

    /**
     * Return error response from mock server
     */
    inner class ErrorDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return when {
                request.path?.equalsToWildcard(RPC_PREAPPLY_OPERATION) == true ->
                    response(500).setBody(getStringFromFile("error_counter.json"))
                else -> error400()
            }
        }
    }

    companion object {
        private const val STRING_ENCODING = "UTF-8"

        fun getStringFromFile(path: String): String {
            try {
                val resourceAsStream: InputStream? = MockDispatcher::class.java.classLoader!!.getResourceAsStream(path)
                return resourceAsStream?.let {
                    getStringFromStream(resourceAsStream)
                } ?: ""
            } catch (exception: IOException) {
                throw RuntimeException(exception)
            }
        }

        @Throws(IOException::class)
        fun getStringFromStream(input: InputStream): String {
            val inputStreamReader = InputStreamReader(
                input,
                STRING_ENCODING
            )
            val bufferedReader = BufferedReader(inputStreamReader)
            val stringBuilder = StringBuilder()
            var line: String? = bufferedReader.readLine()

            do {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            } while (line != null)

            bufferedReader.close()
            inputStreamReader.close()
            return stringBuilder.toString()
        }
    }
}

fun Dispatcher.validResponse() = response(200)

fun Dispatcher.error404() = this.response(404)

fun Dispatcher.error400() = this.response(400)

fun Dispatcher.response(code: Int) = MockResponse()
    .setResponseCode(code)
